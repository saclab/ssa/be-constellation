Ces binaires ont été compilés à partir du dépôt git de ffmpeg :
	https://github.com/FFmpeg/FFmpeg.git
	
La révision utilisée correspond à celle de ffmpeg_lib_linux32 et ffmpeg_lib_win32 :
	commit #ed96241 (du 09-04-2014) -> git checkout ed96241
	
Configuré avec les options :
	--enable-pic
	--disable-static
	--enable-shared
	--disable-yasm
	
Librairies générées depuis un environnement RedHat 5.3 64bits.
La compilation depuis un RedHat plus récent pose problème car FFMpeg utilise la glib, qui est en version 2.5 avec RedHat 5.3 et 2.7 avec RedHat 6
	
(refs #2841)