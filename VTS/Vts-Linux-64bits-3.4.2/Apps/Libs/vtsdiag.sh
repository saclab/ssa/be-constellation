#!/bin/sh
# ---------------------------------------------------------------------------- #
# linux_vtsdiag.sh
#
# HISTORIQUE
# VERSION : 3.4 : DM : DM_508 : 10/12/2019 : Ajout des informations sur le contexte de la visualisation dans le log du Broker
# FIN-HISTORIQUE
# Copyright © (2019) CNES All rights reserved
#
# VER : $Id: vtsdiag.sh 8148 2019-12-10 10:46:01Z mmo $
#
# Exécution du diagnostic VTS sous Linux
# ---------------------------------------------------------------------------- #


CURRENT_DIR=$(dirname $(readlink -f $0))

# ---------------------------------------------------------------------------- #
# LD_LIBRARY_PATH
# ---------------------------------------------------------------------------- #

export LD_LIBRARY_PATH=${CURRENT_DIR}:${LD_LIBRARY_PATH}

# ---------------------------------------------------------------------------- #
# Plugins Qt
# ---------------------------------------------------------------------------- #

export QT_PLUGIN_PATH=${CURRENT_DIR}
export QT_QPA_PLATFORM_PLUGIN_PATH=${CURRENT_DIR}/platforms

# ---------------------------------------------------------------------------- #
# Execution du diagnostic
# ---------------------------------------------------------------------------- #

./vtsdiag $@

# --------------------------------- End Of File ------------------------------ #
