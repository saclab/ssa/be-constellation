#!/bin/sh
# ---------------------------------------------------------------------------- #
# cleanerCelestia.sh
#
# HISTORIQUE
# VERSION : 2.6 : DM : DM_177 : 14/11/2014 : Nettoyage des caches
# FIN-HISTORIQUE
# Copyright � (2014) CNES All rights reserved
#
# VER : $Id: 
#
# Suppression des r�pertoires temporaires de VTS
# ---------------------------------------------------------------------------- #


# Chemin absolu vers le script
SCRIPT=$(readlink -f "$0")
# Chemin absolu du r�pertoire contenant le script
SCRIPTPATH=$(dirname "$SCRIPT")


OPTS_DONE=''
until [ "$OPTS_DONE" != "" ]
do
   case "$1" in
      # �a ressemble � une option
      -*)
         case "$1" in
            "--clear")
               # Suppression des r�pertoires temporaires
               rm -rf $SCRIPTPATH/extras_*
               
               # Code retour
               exit 0
               ;;
               
         esac
         ;;

      # �a ne ressemble pas � une option : fin des options
      *)
         OPTS_DONE='true'
         ;;
   esac
done


# ---------------------------------------------------------------------------- #
