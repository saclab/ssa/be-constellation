# Requirement

lsb_release -a
Ubuntu 22.04

## Install docker
```
sudo apt-get update
(sudo apt-get upgrade)

sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
```

## Install docker-compose

### Add Docker’s official GPG key:

```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```
### Add Docker compose repo

```
sudo add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable"

sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
sudo apt-get install docker-compose
```

## Add user to docker group if not done
sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker


## Check docker installation
sudo docker run hello-world

# Quick demo
##getting all in your $HOME
in your download place: 

```
cd $HOME
tar -zxvf JSatorb-stand-alone-v01.tar.gz
mv JSatorb-stand-alone-v01 JSatorb
cd jsatorb-docker-demo
```
OR
cd $HOME
git clone  https://sourceforge.isae.fr/git/jsatorb-docker-demo
mv JSatorb-stand-alone-v01 JSatorb
cd JSatorb

##loading docker images, starting them into container, launching a  browser...
```
bash load-docker-images.bash
bash jsatorb-start.bash
```

##docker-compose version

at this point, if there is a "docker-compose" issue linked with docker-compose.yml, simply replace "version number"...
from '3.8' to '3', first line of docker-compose.yml.


##GUI
At this time, a browser should be running (as well as 3 docker container)
Actions:
(in the browser: http://localhost/)
choose TLE (top left)
Time setting:
 - duration 8600
 - Step Time 60

click on "celestrak import TLE" (top fright)
in filter, write "iridium" and pick the first satellite appearing (check box activated)
click on "validate TLE Selection"
click on "<" top right (going back to cesium viewer)
click on "Run mission Analysis"
Intercat with Cesium viewer (hold right click to turn around, hold left click to zoom in/out, accelerate or hold simulation, move in time...)

From this point, you can modify name of the satellite, their TLE, add more satellites, define your own satellite. - little pen on satellite Name, in TLE list.
Be sure to stay in TLE mode (there is not yet inter-compatibility with Keplerian mode, even at the end, data is the same !)

enjoy !

##To stop in a clean way the docker containers:

```
bash jsatorb-stop.bash

```

# Troubleshot on docker install :


```

sudo apt-get remove docker docker-engine docker.io containerd runc
sudo chmod +x /usr/local/bin/docker-compose

```

# Fedora

```
sudo dnf -y install dnf-plugins-core
sudo dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
sudo dnf install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

sudo systemctl start docker

## Add user to docker group if not done
sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker

## Check docker installation
docker run hello-world


sudo dnf install docker-compose


## addsuer
sudo adduser jsatorb
sudo passwd jsatorb
js

# sudo rights
#usermod -aG wheel jsatorb



```

https://www.if-not-true-then-false.com/2011/nautilus-open-in-terminal-on-fedora-centos-red-hat-rhel/


Code:

#!/usr/bin/sh
./filename

where filename is name of your program's executable assuming that script is placed in the same folder as the executable. You may name the text file any way, .BAT or any other extension is not needed, but you have to mark it executable. It could be done either in right-click -> properties ("rights" -> "allow to execute" or so on) or by typing

Code:

chmod +x script


#docker run -it --mount type=bind,source=/home/$USER/JSatOrb/JSatOrbAgent/cache,target=/app/Data --entrypoint /bin/bash --rm -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=unix$DISPLAY --network=host --name vts-container vts:dev



VTS 3.4.2 build 8200 arch 64



