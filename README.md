# BE_constellation


## JSatrob download

A compressed archive is accessible: [ JSatOrb_latest.tar.gz ](https://gitlab.isae-supaero.fr/nanostar/jsatorb/-/packages/).

## Requirement

To check the installed version of the operating sytem, in a terminal, enter the following command line:

```
lsb_release -a
```

Ubuntu 22.04

## Install docker

To install `docker`, in a terminal, enter the following command lines:

```
sudo apt-get update
(sudo apt-get upgrade)

sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
```

## Install docker-compose

To install `docker-compose`, in a terminal, enter the following command lines.

### Add Docker’s official GPG key:

```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```
### Add Docker compose repo

```
sudo add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable"

sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
sudo apt-get install docker-compose
```

## Add user to docker group
```
sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker
sudo systemctl restart docker
reboot
sudo chown $USER /var/run/docker.sock
```

## Check docker installation
```
sudo docker run hello-world
```
## VTS additional libraries

To check if those additional libraries/packages are available, run the following commands:
```
apt-cache show libjpeg62
apt-cache show libjpeg62:i386
apt-cache show libpng12-0
apt-cache show gcc-multilib
apt-cache show libx11-6:i386
```

__If one of those packages is missing, you have to:__

- Either call an ISAE Administrator (see the dev-detailed-install-procedure.md, or look into the shortened installation instructions below),
- Or if you are familiar with installing software through the apt tool, you can install the needed packages yourself:

    Install the needed packages:
    ```
    sudo apt update
    sudo apt install build-essential zlib1g-dev
    sudo apt install libjpeg62
    sudo apt-get install libjpeg62:i386
    sudo apt install gcc-multilib
    sudo apt install libx11-6:i386

    cd
    mkdir src
    wget https://ppa.launchpadcontent.net/linuxuprising/libpng12/ubuntu/pool/main/libp/libpng/libpng_1.2.54.orig.tar.xz
    tar Jxfv libpng_1.2.54.orig.tar.xz
    cd libpng-1.2.54
    ./configure
    make
    sudo make install
    sudo ln -s /usr/local/lib/libpng12.so.0.54.0 /usr/lib/libpng12.so
    sudo ln -s /usr/local/lib/libpng12.so.0.54.0 /usr/lib/libpng12.so.0


# Quick demo

## Getting all JSatorb/ in your $HOME
In your download place of `JSatOrb_latest.tar.gz`: 

```
tar -xvzf JSatOrb_latest.tar.gz -C /home/$USER/
mv JSatorb /home/$USER/
cd ~/JSatOrb/

```

## Loading docker images, starting them into container, launching a  browser...
```
bash jsatorb-load-docker-images.bash
bash jsatorb-start.bash
```
In a browser, go to : 

[ localhost ](http://localhost/)

If it's not launching, hight probability is an incorrect docker persmission settings, see below.


### docker-compose troubleshooting

In 3 terminals: 

```
docker run -p 80:80 -it --rm jsatorb-frontend:hotFix
docker run -it --rm -p 8000:8000 -v "/home/$USER/JSatOrb/mission-data:/root/JSatOrb/mission-data" --name jsatorb-backend-container jsatorb-backend:prod
docker run -it --rm -p 7777:7777 --name celestrak-json-proxy-container thib21/celestrak-json-proxy:dev
```

## docker-compose version

at this point, if there is a "docker-compose" issue linked with docker-compose.yml, simply replace "version number"...
from '3.8' to '3', first line of docker-compose.yml.

## at first start

follow official procedure to link VTS directly with VTS agent in firefox:
https://gitlab.isae-supaero.fr/nanostar/jsatorb#2-important-configuring-firefox-to-associate-jsatorb-agent-with-vz-files



# Quickstart with GUI - testing VTS

Actions:
(in the browser: http://localhost/)

# Quickstart with GUI - testing Celestrak

At this time, a browser should be running (as well as 3 docker container)

To use JSatOrb: 

1) In the browser at address http://localhost/, enter Time Settings (top left) for TLE in `Time Settings` box by selecting a `Starting date`, an `Ending Date` (for example, choose 8600 seconds between start and end datetime) and a `Step Time` of 60 seconds.

2) Click on `import celestrak TLE` (top right). 
3) In filter, write "iridium" and pick the first satellite appearing (check box activated)
4) Click on `Validate TLE Selection`, the selected satellite should appear in `Satellites list` on left menu.
5) Click on `<` top right (going back to Cesium viewer)
6) Click on `Run mission Analysis`
7) Interact with Cesium viewer (hold right click to turn around, hold left click to zoom in/out, accelerate or hold simulation, move in time...)

From this point, you can modify name of the satellite, their TLE, add more satellites, define your own satellite. - little pen on satellite Name, in TLE list.
Be sure to stay in TLE mode (there is not yet inter-compatibility with Keplerian mode, even at the end, data is the same !)

enjoy !

## Stop JSatOrb

This command line will stop in a clean way the docker containers:
```
bash jsatorb-stop.bash
```

### VTS manual launch

```
cd /home/$USER/JSatOrb/Vts-Linux-64bits-3.4.2
./vts-start
```

# Troubleshooting

## Docker install :

```
sudo apt-get remove docker docker-engine docker.io containerd runc
sudo chmod +x /usr/local/bin/docker-compose
```

## HotFix for frontend: 

because
http://celestrak.org/NORAD/elements/gp.php?GROUP=active&FORMAT=tle 
changed API

```
docker stop jsatorb-frontend-container
docker rm jsatorb-frontend-container
docker run -p 80:80 -d --name jsatorb-frontend-fix-container gitlab-registry.isae-supaero.fr/jsatorb-dev/jsatorb-frontend:hotFix 
```

## HotFix for frontend alternative

```
docker build  . -t jsatorb-frontend:hotFix

#test
docker build  . -t jsatorb-frontend:hotFix
docker run -p 80:80 -it --rm jsatorb-frontend-luplink:hotfix
```

## VTS LIBGL issue (Docker graphics)

```
LIBGL_ALWAYS_SOFTWARE=1 ./startVTS
```

## Prestoplot Troubleshoot

```
export LANG=fr_FR.UTF-8 
```

## Scilab 6.0.2 Troubleshoot

https://forum.ubuntu-fr.org/viewtopic.php?id=2051849

```
LIBGL_ALWAYS_SOFTWARE=1 ./scilab

--> exec('/home/$USER/Utils/celestlabx/loader.sce', -1)
```

## Fedora specific install (link with ICARES)

```
sudo dnf -y install dnf-plugins-core
sudo dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
sudo dnf install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

sudo systemctl start docker

## Add user to docker group if not done
sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker

## Check docker installation
docker run hello-world

sudo dnf install docker-compose

## addsuer
sudo adduser jsatorb
sudo passwd jsatorb
js

# sudo rights
#usermod -aG wheel jsatorb


```

https://www.if-not-true-then-false.com/2011/nautilus-open-in-terminal-on-fedora-centos-red-hat-rhel/


Code:

```
#!/usr/bin/sh ./filename
```

where ```filename``` is name of your program's executable assuming that script is placed in the same folder as the executable. You may name the text file any way, .BAT or any other extension is not needed, but you have to mark it executable. It could be done either in right-click -> properties (```rights``` -> ```allow to execute``` or so on) or by typing

Code:

```
chmod +x script
```

```
#docker run -it --mount type=bind,source=/home/\$USER/JSatOrb/JSatOrbAgent/cache,target=/app/Data --entrypoint /bin/bash --rm -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=unix$DISPLAY --network=host --name vts-container vts:dev
```

